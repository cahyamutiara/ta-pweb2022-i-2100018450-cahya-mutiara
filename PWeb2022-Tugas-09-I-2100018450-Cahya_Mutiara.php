<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tugas Pemrograman Web</title>
</head>
<body>
	<h3><b>Listing Program 9.1</b></h3>
		<?php 
			$gaji = 1000000;
			$pajak = 0.1;
			$thp = $gaji - ($gaji * $pajak);

			echo "Gaji sebelum pajak = Rp. $gaji<br>";
			echo "Gaji yang dibawa pulang = Rp. $thp";
		?>
	<br><br>
	<h3><b>Listing Program 9.2</b></h3>
		<?php 
			$a = 5;
			$b = 4;

			echo "$a == $b : ". ($a == $b);
			echo "<br>$a != $b : ". ($a != $b);
			echo "<br>$a > $b : ". ($a > $b);
			echo "<br>$a < $b : ". ($a < $b);
			echo "<br>($a == $b) && ($a > $b) : ". (($a != $b) && ($a > $b));
			echo "<br>($a == $b) || ($a > $b) : ". (($a != $b) || ($a > $b));
		 ?>
</body>
</html>