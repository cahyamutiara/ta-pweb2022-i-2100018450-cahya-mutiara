<?php

$personalDatabasePath = getcwd() . DIRECTORY_SEPARATOR .'personal.db.json';
$bookDatabasePath = getcwd() . DIRECTORY_SEPARATOR .'book.db.json';
$mode = $_POST['formId'];


$getPersonalRequest = fn() => array(
    'name' => $_POST['name'],
    'gender' => $_POST["gender"],
    'address'          =>     $_POST["address"],
    'email'     =>     $_POST["email"],
    'phone'     =>     $_POST["phone"]
);

$getBookRequest = fn() => array(
    'booktitle' => $_POST['booktitle'],
    'writer' => $_POST["writer"],
    'synopsis'          =>     $_POST["synopsis"],
    'review'     =>     $_POST["review"],
    'rating'     =>     (int)$_POST["rating"]
);
if ($mode == "personal") {
    updateDatabase($personalDatabasePath, $getPersonalRequest);
} else {
    updateDatabase($bookDatabasePath, $getBookRequest);
}

function updateDatabase($databasePath, $getCurrentDataFunc) {
if(file_exists($databasePath) && file_get_contents($databasePath) != null)
    {
        $final_data=fileWriteAppend($databasePath, $getCurrentDataFunc);
        file_put_contents($databasePath, $final_data);
        echo json_encode(array(
			'success' => 1,
			'message' => 'Data telah terupdate!'
		));
    } else
    {
        $final_data=fileCreateWrite($databasePath, $getCurrentDataFunc);
        file_put_contents($databasePath, $final_data);
        echo json_encode(array(
			'success' => 1,
			'message' => 'Data telah terupdate!'
		));
    }
}
function fileWriteAppend($databasePath, $getCurrentDataFunc) {
		$current_data = file_get_contents($databasePath);
		$array_data = json_decode($current_data, true);
        $final_array = array_merge($array_data, array($getCurrentDataFunc()));
		$final_data = json_encode($final_array);
		return $final_data;
}
function fileCreateWrite($databasePath, $getCurrentDataFunc){
		$file = fopen($databasePath, "w");
		$final_data = json_encode(array($getCurrentDataFunc()));
		fclose($file);
		return $final_data;
}
