<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UTS Pemrograman Web</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
	<header>
		<nav>
			<ul>
				<li><a href="" class="active">Home</a></li>
				<li><a href="">Search</a></li>
				<li><a href="">About Us</a></li>
			</ul>
		</nav>
		<div class="title">
			<h1>WELCOME</h1>
		</div>
		<div class="button">
			<a href="profile.php" class="btn">Data Diri</a>
			<a href="index.php" class="btn">Form</a>
			<a href="resensi.php" class="btn">Resensi</a>
		</div>
	</header>
	<main>
		<article>
			<div class="content">
				<h3 id="profile">PROFILE</h3>
				<p>Nama : Cahya Mutiara</p>
				<p>NIM : 2100018450</p>
				<p>Kelas : I</p><br>
				<p ><i>Web ini dibuat dengan tujuan untuk memenuhi tugas mata kuliah Pemrograman Web.</i></p><br>
			</div>
			<div class="content">
				<h3 id="form">FORM DATA DIRI</h3>
					<form id="myform" method="POST">
					<input type="hidden" id="formId" name="formId" value="personal">
						<label for="name">Nama :</label>
							<input type="text" id="name" name="name"><br><br>
						<label for="gender">Jenis Kelamin :</label>
							<select id="gender" name="gender"><br><br>
							<option value="Laki-laki">Laki-Laki</option>
							<option value="Perempuan">Perempuan</option>
							</select>
						<p>Alamat :</p>
						<textarea id="address" name="address" rows="4" cols="50"></textarea><br><br>
						<label for="email">Email :</label>
							<input type="text" id="email" name="email"><br><br>
						<label for="phone">No. HP :</label>
							<input type="text" id="phone" name="phone"><br><br>
						<input type="button" value="Submit" onclick="submitPersonal()">
						<input type="reset" value="Clear">
					</form><br>
			</div>
			<div class="content">
			<h3 id="resensi">RESENSI BUKU</h3>
				<form id="bookform" action="form.php" method="POST">
					<input type="hidden" id="formId" name="formId" value="books">
					<p>Judul Buku :</p>
					<textarea id="booktitle" name="booktitle" rows="1" cols="50"></textarea>
					<p>Penulis Buku :</p>
					<textarea id="writer" name="writer" rows="1" cols="50"></textarea>
					<p>Sinopsis Buku :</p>
					<textarea id="synopsis" name="synopsis" rows="5" cols="50"></textarea>
					<p>Ulasan Buku :</p>
					<textarea id="review" name="review" rows="10" cols="50"></textarea>
					<p>Rating Buku :</p>
					<input type="radio" id="rating1" name="rating" value="1">
					<label for="rating1">⭐</label><br>
					<input type="radio" id="rating2" name="rating" value="2">
					<label for="rating2">⭐⭐</label><br>
					<input type="radio" id="rating3" name="rating" value="3">
					<label for="rating3">⭐⭐⭐</label><br>
					<input type="radio" id="rating4" name="rating" value="4">
					<label for="rating4">⭐⭐⭐⭐</label><br>
					<input type="radio" id="rating5" name="rating" value="5">
					<label for="rating5">⭐⭐⭐⭐⭐</label><br><br>
					<input type="button" value="Submit" onclick="submitBook()">
					<input type="reset" value="Clear">
					<br><br>
				</form>
		</article>
		<aside>
			<div class="sidebar">
				<p align="center">
				<img src="unnamed.png" width="150" height="150"><p><br>
				<h3 align="center">Universitas Ahmad Dahlan</h3>
				<p align="justify">Universitas Ahmad Dahlan (UAD) merupakan pengembangan dari Institut Keguruan dan llmu Pendidikan (IKIP) Muhammadiyah Yogyakarta. Institut Keguruan dan llmu Pendidikan Muhammadiyah Yogyakarta sebagai lembaga pendidikan tinggi merupakan pengembangan FKIP Muhammadiyah Cabang Jakarta di Yogyakarta yang didirikan pada tanggal 18 November 1960. FKIP Muhammadiyah merupakan kelanjutan kursus BI Muhammadiyah di Yogyakarta yang didirikan tahun 1957. Pada waktu itu kursus BI memiliki jurusan Ilmu Mendidik, Civic Hukum dan Ekonomi.</p>
				<p align="justify">Pada tanggal 19 Desember 1994 dengan Surat Keputusan (SK) Menteri Pendidikan dan Kebudayaan Republik Indonesia Nomor: 102/D/0/1994 ditetapkan bahwa IKIP Muhammadiyah Yogyakarta beralih fungsi menjadi Universitas Ahmad Dahlan.</p>
			</div>
		</aside>
	</main>
	<footer>
		<p>Copyright © 2022 Cahya</p>
	</footer>
	<script type="text/javascript">
		function validateBookForm() {
			var booktitle = document.getElementById("booktitle").value;
			var writer = document.getElementById('writer').value;
			var synopsis = document.getElementById("synopsis").value;
			var review = document.getElementById("review").value;
			if (booktitle != "" && writer != "" && synopsis != "" && review != "") {
				return true;
			} else {
				return false;
			}
		}

		function validatePersonalForm() {
			var name = document.getElementById("name").value;
			var address = document.getElementById('address').value;
			var email = document.getElementById("email").value;
			var phone = document.getElementById("phone").value;
			if (name != "" && address != "" && email != "" && phone != "") {
				return true;
			} else {
				return false;
			}
		}

		function submitPersonal() {
			if (validatePersonalForm()) {
				$.ajax( {
					data: $('#myform').serialize(),
					type: "post",
					url: 'form.php',
					success: function(response) {
						var jsonData = JSON.parse(response);
						if (jsonData.success == "1")
						{
							alert(`SUKSES: ${jsonData.message}`);
						} else {
							alert(`GAGAL: ${jsonData.message}`);
						}
					}
    				});
			} else {
				alert ('Anda harus mengisi data dengan lengkap!');
			}
		}

		function submitBook() {
			if (validateBookForm()) {
				$.ajax( {
					data: $('#bookform').serialize(),
					type: "post",
					url: 'form.php',
					success: function(response) {
						var jsonData = JSON.parse(response);
						if (jsonData.success == "1")
						{
							alert(`SUKSES: ${jsonData.message}`);
						} else {
							alert(`GAGAL: ${jsonData.message}`);
						}
					}
    				});
			} else {
				alert ('Anda harus mengisi data dengan lengkap!');
			}
		}
	</script> 
</body>
</html>