<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UTS Pemrograman Web</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<header>
		<nav>
			<ul>
				<li><a href="" class="active">Home</a></li>
				<li><a href="">Search</a></li>
				<li><a href="">About Us</a></li>
			</ul>
		</nav>
		<div class="title">
			<h1>WELCOME</h1>
		</div>
		<div class="button">
			<a href="profile.php" class="btn">Data Diri</a>
			<a href="index.php" class="btn">Form</a>
			<a href="resensi.php" class="btn">Resensi</a>
		</div>
	</header>
	<main>
		<article>
			<div class="content">
				<h3 id="profile">PROFILE</h3>
				<p>Nama : Cahya Mutiara</p>
				<p>NIM : 2100018450</p>
				<p>Kelas : I</p><br>
				<p ><i>Web ini dibuat dengan tujuan untuk memenuhi tugas mata kuliah Pemrograman Web.</i></p><br>
			</div>
			<div class="content">
				<h3 id="form">TABEL DATA DIRI</h3>
				<table class="styled-table">
					<thead>
						<tr>
							<th>Nama</th>
							<th>Jenis Kelamin</th>
							<th>Alamat</th>
							<th>Nomor Telepon</th>
							<th>Email</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$jsondata = file_get_contents(getcwd() . DIRECTORY_SEPARATOR .'personal.db.json');
						$results = json_decode($jsondata, false);
						if ($results) {
							foreach ($results as $r) {
								echo "<tr>";
								echo "<td>$r->name</td>";
								echo "<td>$r->gender</td>";
								echo "<td>$r->address</td>";
								echo "<td>$r->phone</td>";
								echo "<td>$r->email</td>";
								echo "</tr>";
							} 
						} else {
							echo '<tr><td colspan="5">Belum ada profil!</td></tr>';
						}
						?>
					</tbody>
				</table>
			</div>
		</article>
		<aside>
			<div class="sidebar">
				<p align="center">
				<img src="unnamed.png" width="150" height="150"><p><br>
				<h3 align="center">Universitas Ahmad Dahlan</h3>
				<p align="justify">Universitas Ahmad Dahlan (UAD) merupakan pengembangan dari Institut Keguruan dan llmu Pendidikan (IKIP) Muhammadiyah Yogyakarta. Institut Keguruan dan llmu Pendidikan Muhammadiyah Yogyakarta sebagai lembaga pendidikan tinggi merupakan pengembangan FKIP Muhammadiyah Cabang Jakarta di Yogyakarta yang didirikan pada tanggal 18 November 1960. FKIP Muhammadiyah merupakan kelanjutan kursus BI Muhammadiyah di Yogyakarta yang didirikan tahun 1957. Pada waktu itu kursus BI memiliki jurusan Ilmu Mendidik, Civic Hukum dan Ekonomi.</p>
				<p align="justify">Pada tanggal 19 Desember 1994 dengan Surat Keputusan (SK) Menteri Pendidikan dan Kebudayaan Republik Indonesia Nomor: 102/D/0/1994 ditetapkan bahwa IKIP Muhammadiyah Yogyakarta beralih fungsi menjadi Universitas Ahmad Dahlan.</p>
			</div>
		</aside>
	</main>
	<footer>
		<p>Copyright © 2022 Cahya</p>
	</footer>
</body>
</html>